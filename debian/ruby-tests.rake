# Tests must be run in a UTF-8 environment
ENV['LC_ALL'] = 'C.UTF-8'

require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec)

task :default => :spec
